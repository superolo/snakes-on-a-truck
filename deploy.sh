#!/bin/bash
if [ -d /home/gitlab-runner/.aws ]
then
  echo 'aws dir exists - do nothing'
else
  mkdir -p /home/gitlab-runner/.aws
fi

if [ -d /home/gitlab-runner/.kube ]
then
  echo 'kuba dir exists - do nothing'
else
  mkdir -p /home/gitlab-runner/.kube
fi

echo ${KUBA} | base64 -d > /home/gitlab-runner/.kube/config
echo ${SAILER} | base64 -d > /home/gitlab-runner/.aws/credentials


echo 'deploying module_1'
kubectl apply -f $CI_PROJECT_DIR/module_1/kubernetes/
echo 'done deploying  module_1'

echo 'deploying module_2'
kubectl apply -f $CI_PROJECT_DIR/module_2/kubernetes/
echo 'done deploying  module_2'

echo 'deploying module_base'
kubectl apply -f $CI_PROJECT_DIR/module_base/kubernetes/
echo 'done deploying  module_base'