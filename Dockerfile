FROM alpine:3.6
MAINTAINER Mr Ohai <ohai@deliver-stuff.com>

RUN apk add --no-cache \
  bash \
  curl \
  grep \
  jq

COPY merge-request.sh /usr/bin/

CMD ["merge-request.sh"]
