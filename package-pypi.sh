#!/usr/bin/env bash
cd $CI_PROJECT_DIR 
python3 -m pip install --user --upgrade setuptools wheel
python3 -m pip install --user --upgrade twine

echo 'about to upload'
python3 setup.py sdist bdist_wheel upload -r testpypi