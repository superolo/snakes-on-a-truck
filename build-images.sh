#!/bin/bash

APP_VERSION=$(cd $CI_PROJECT_DIR && git show origin/master:setup.py | grep -oP "(?<=version\=')\d+\.\d+\.\d+")

echo 'building module 1'
cd $CI_PROJECT_DIR/module_1 
sudo docker build -t registry.gitlab.com/superolo/modules/module_one:$APP_VERSION .
sudo docker login -u deliver-snakes -p $GITLAB_PRIVATE_TOKEN  registry.gitlab.com
sudo docker push registry.gitlab.com/superolo/modules/module_one:$APP_VERSION
echo 'done building module 1'


echo 'building module 2'
cd $CI_PROJECT_DIR/module_2 
sudo docker build -t registry.gitlab.com/superolo/modules/module_two:$APP_VERSION .
sudo docker login -u deliver-snakes -p $GITLAB_PRIVATE_TOKEN  registry.gitlab.com
sudo docker push registry.gitlab.com/superolo/modules/module_two:$APP_VERSION
echo 'done building module 2'

echo 'building module base'
cd $CI_PROJECT_DIR/module_base 
sudo docker build -t registry.gitlab.com/superolo/modules/module_base:$APP_VERSION .
sudo docker login -u deliver-snakes -p $GITLAB_PRIVATE_TOKEN  registry.gitlab.com
sudo docker push registry.gitlab.com/superolo/modules/module_base:$APP_VERSION
echo 'done building module base'