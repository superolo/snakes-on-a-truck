#!/bin/bash
version_regex="(?<=version\=')\d+\.\d+\.\d+"
setup_file='setup.py'
master_version=$(cd $CI_PROJECT_DIR && git show origin/master:$setup_file | grep -oP "$version_regex")
branch_version=$(cd $CI_PROJECT_DIR && git show $CI_COMMIT_SHA:$setup_file | grep -oP "$version_regex")

if [ $CI_COMMIT_REF_NAME != 'master' ]
then
  if [ $master_version != $branch_version ]
  then
    echo 'OK'
  else
    echo "ERROR: You haven't changed versions"
    exit 1
  fi
else
  if [ $master_version == $branch_version ]
  then
    echo 'OK'
  else
    echo 'something went wrong!'
    exit 1
  fi
fi